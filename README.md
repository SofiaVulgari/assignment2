# Assignment2 

Assignment 2. Create a Sign Language Translator using React

## Install

```
git clone https://gitlab.com/SofiaVulgari/assignment2
cd assignment2
npm install
```

## Usage

**API:**
- Create an .env file in the root-directory (Same level as the README.md file and package.json) and add the following two lines.
- Exchange "Your key" with the API-key.
- Exchange "Your URL" with the API URL.

```
REACT_APP_API_KEY= Your key
REACT_APP_API_URL= Your URL
```
```
npm start
```

## Contributors

This assignment has been done by [Sofia Vulgari @SofiaVulgari](@SofiaVulgari) and [Mattias Eriksson @MattiasLeifEriksson](@MattiasLeifEriksson)
