const apiKey = process.env.REACT_APP_API_KEY

//reused function for apikey  
export const createHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'x-api-key': apiKey

    }
}
