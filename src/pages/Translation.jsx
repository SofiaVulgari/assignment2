import "../pages/translation.css";
import React from "react";
import { useState } from "react";
import { addTranslation } from "../api/translation";
import { useUser } from "../context/UserContext";
import { TranslatingWord } from "../components/Translation/TranslatingWord"

/**
 *  Function that handles the translation page.
 */
function Translation() {
  const { user } = useUser() //Logged in user.
  let key = 0; //Variable used to get a unique key-value for each image.
  const [translatedWord, translate] = useState(""); //The word that the user inputs into the text-field, which will get translated
  const [check, setCheck] = useState(false); //Checks to make sure that the user has submitted the form.
  const [signSource, setSource] = useState("") //The images sources.
  /*HandleChange updates translatedWord each time they type something in the text-field */
  const handleChange = (event) => { 
    translate({ value: event.target.value })
  }
  /**
   * Gets called on submit. Handles the word that the user has inputed. 
  */
  const handleSentence = async (event) => {
    event.preventDefault(); //Stops the form from refreshing the page.
    const myWords = translatedWord.value

    const [error, result] = await addTranslation(user, myWords)
    const signs = TranslatingWord(translatedWord.value); //TranslatingWord handles the word, for example filtering out characters.
    setSource(signs); //Sets signs as source. Signs is an array.
    setCheck(true); //The form is submitted so check is set to True.
    console.log('error', error)
    console.log('result', result)

  }

  /**
   * The translation-part of the react-app
   */
  return ( 
    <div className="Translation">
      <header className="myHeader"><img src={require("../img/Logo-Hello.png")} className="myLogo" alt="logo" height={100} width={100} />Lost in Translation</header>
      <h1 className="font-weight-light">What would you like to translate?</h1>
      <form onSubmit={handleSentence}>
        <input type="text" name="WordsToTranslate" id="WordsToTranslate" onChange={handleChange} placeholder="Hello..."></input>
        <button className="TranslateSubmit" id="TranslateSubmit" >Translate</button>
      </form>

      <div id="TranslationWindow"> 
        {check && (

          signSource.map(((source) => { //Goes through the sign-array and then gets the correct images.
            key++; 
            return (<img width={50} height={50} key={key} src={require("../signs/" + source + ".png")} alt="" />)
          }))

        )

        }
      </div>
    </div>
  )

}
export default Translation;
