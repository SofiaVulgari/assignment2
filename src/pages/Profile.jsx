import TranslateHistory from "../components/Profile/TranslateHistory"
import { useUser } from "../context/UserContext"
/**
 * The profile-section of the website. Used to mainly display the translation history of a user. 
*/
const Profile = () => {

  const { user } = useUser()
  return (
    <>
      <TranslateHistory translations={user.translations} />
    </>
  )
}
export default Profile
