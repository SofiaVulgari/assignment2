import LoginForm from "../components/Login/LoginForm"

/**
 * The Start-up part of the website. Gets displayed directly when the user goes into the website for the first time. Contains the LoginForm so the user can log in.
 */
const StartUp = () => { 
  return (
    <>
      <LoginForm /> 
    </>
  )
}

export default StartUp
