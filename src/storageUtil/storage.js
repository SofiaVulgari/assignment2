/**
 * Saves data in the localstorage
*/
export const storageSave = (key, value) => {
    console.log(key)

    localStorage.setItem(key, JSON.stringify(value))
    console.log(key)

}
/**
 * Gets and read data from the localstorage.  
*/
export const storageRead = key => {
    console.log(key)
    const data = localStorage.getItem(key)
    if (data) {
        return JSON.parse(data)
    }
    console.log(key)

    return null
}

export const storageDelete = key => {
    localStorage.removeItem(key)
}
