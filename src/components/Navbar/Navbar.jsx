import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext"

/**
 * The navbar that lets the user switch between translation-page and the profile-page if they are logged in 
*/
const Navbar = () => {

    const { user } = useUser()

    return (
        <nav>
            {(user !== null) &&
                <ul>
                    <li>
                        <NavLink to="/translation">Translate</NavLink>
                    </li>
                    <li>
                        <NavLink to="/profile">My Profile</NavLink>
                    </li>
                </ul>
            }
        </nav>
    )
}
export default Navbar
