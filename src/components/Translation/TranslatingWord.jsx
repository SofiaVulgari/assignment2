/**
 * Gets the word from the form in the translation.js and matches each letter to an image. Returns an array with all the images.
 */
export function TranslatingWord(word) { 
    const signArray = []; //Array for each character
    const regex = /[a-z]/i; //Regex that checks for letter-characters a-z.
    word = word.toString().toLowerCase(); //Changes any uppercase-characters to lowercase.
    word = word.split(""); //Splits the word.
    for (let i = 0; word.length > i; i++) { //Goes through every letter in the word and checks if they match with the regex. If they are a match, they will get pushed into signArray and sent back to translation.js. Else it will just print a console.log
        if (word[i].match(regex)) {
            signArray.push(word[i])
        }
        else {
            console.log("Num" + word[i]);
        }
    }
    return signArray;
}
