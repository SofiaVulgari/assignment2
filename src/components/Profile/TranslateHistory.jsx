import React from "react";
import "../../components/Profile/profile.css"
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave } from "../../storageUtil/storage"
import HistoryItem from "./HistoryItem";
import { translationClearHistory } from "../../api/translation";


const TranslateHistory = ({ translations }) => {

    const { user, setUser } = useUser()
    const navigate = useNavigate();
    /**
     * Handles logout when the user clicks the log-out-button. Deletes the user from storage and redirects to the login-page. 
    */
    const handleLogOut = () => {
        if (window.confirm('You will be logged out. Are you sure?')) {
            storageDelete('user')
            setUser(null)
            navigate("/.");
        }
    }
    /**
     * HandleClearHistory, clears the array with previous translations when the users click the clear-button. 
    */
    const handleClearHistory = async () => {
        if (!window.confirm('Your translation history will be lost. Are you sure?')) {
            return
        }

        const [clearError] = await translationClearHistory(user.id)

        if (clearError !== null) {
            console.log('error', clearError)
        }

        storageSave('user', {
            ...user,
            translations: []
        })
        setUser({
            ...user,
            translations: []
        })
    }
    if (translations.length > 10) { //Limits the shown translations to only the last 10 if there are more than 10.
        translations = translations.slice(-10);
    }
    /**
     * Goes through all the translations in the array and displays them.
    */
    const translationList = translations.map((myWords, index) => <HistoryItem key={index + '-' + myWords} myWords={myWords} />)
    /**
     * Displays the profile-page, featuring the translation-history, logout-button and clear button.
    */
    return (
        <div className="Profile">
            <header className="myHeader"><img src={require("../../img/Logo.png")} className="myLogo" alt="logo" height={100} width={100} />Lost in Translation</header>
            <h2 className="welcome"> Welcome to your profile {user.username}!</h2>

            <div id="translationsHistory">
                <h3 className="lastSearches">Your last searches: </h3>
                <ul className="history">
                    {translationList}
                </ul>
            </div>
            <button className="btn" id="clearBtn" onClick={handleClearHistory}>Clear</button>
            <button className="btn" id="logout" onClick={handleLogOut}>Log out</button>
        </div>
    )

}
export default TranslateHistory;
