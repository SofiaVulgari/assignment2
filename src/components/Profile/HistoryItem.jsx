/**
 * Adds the translated words to an li-element and then exports them so they can be displayed.
 */
const HistoryItem = ({ myWords }) => {
    return <li>{myWords}</li>
}
export default HistoryItem
