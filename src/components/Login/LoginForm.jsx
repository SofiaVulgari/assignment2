import "./loginForm.css";
import { useForm } from "react-hook-form"
import { useEffect, useState } from "react"
import { loginUser } from '../../api/user'
import { storageSave } from "../../storageUtil/storage"
import { useNavigate } from "react-router-dom"
import { useUser } from "../../context/UserContext"

//The user is required to login in order to proceed.
const usernameConfig = {
    required: true
}
/**
 * The function that handles the login of a user.
*/
const LoginForm = () => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { user, setUser } = useUser() //sets the user
    const navigate = useNavigate() //Enables navigation between pages

    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)

    /**
     * If the user is logged in, they can navigate from the profile to the translation-page. 
     **/
    useEffect(() => {
        if (user !== null) {
            navigate('/translation')
        }
    }, [user, navigate])

    /** 
     * Handles the username/login after submit.If there is an error, it will get displayed on the page, otherwise it will save the user in the localstorage and in the API.
    */
    const onSubmit = async ({ username }) => {
        setLoading(true);
        const [error, userResponse] = await loginUser(username)
        console.log(userResponse)

        if (error != null) {
            setApiError(error)
        }
        if (userResponse !== null) {
            console.log(userResponse)
            storageSave('user', userResponse)
            setUser(userResponse);
        }
        setLoading(false);
    }

    /**
     * If there are error messages, they will be displayed on the page. 
    */
    const errorMsg = (() => {
        if (!errors.username) {
            return null
        }
        if (errors.username.type === 'required') {
            return <span>Username is required</span>
        }
    })();

    /** 
     * The login-page. Includes a form were the user can input their name/username and then submit.
     */
    return (
        <div className="startup">
            <header className="myHeader"><img src={require("../../img/Logo.png")} className="myLogo" alt="logo" height={100} width={100} />Lost in Translation</header>
            <section className="mainBody">
                <h1 className="font-weight-light">Get started:</h1>

                <form onSubmit={handleSubmit(onSubmit)}>

                    <label htmlFor="username"></label>
                    <input type="text" placeholder="What's your name?" {...register("username", usernameConfig)} />

                    {errorMsg}

                    <button className="btnlogin" type="submit" disabled={loading} >Login</button>

                    {loading && <p>Loading...</p>}
                    {apiError && <p>{apiError}</p>}
                </form>
            </section>
        </div>
    )
}

export default LoginForm


